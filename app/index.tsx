import React, {useContext, useEffect, useState} from 'react';
import {
  Button,
  NavigationButton,
  NavigationContainer,
  NavigationContext,
  ScreenContainer,
  ScreenContainerProps,
  SizedBox,
  Styles,
  Theme,
} from '@company/kits';
import {
  IconButtonUsage,
  NavigationUsage,
  TextUsage,
  ThemeUsage,
} from '@screens';
import {Alert, DeviceEventEmitter} from 'react-native';
import {Images} from '@configs';
import {HeaderRightAction} from '@company/kits/Navigation/Components';
import {HeaderButtonProps} from '@react-navigation/native-stack/src/types';

const appTheme: Theme = {
  dark: false,
  colors: {
    primary: '#eb2f96',
    secondary: '#007aff',
    background: '#f9f9f9',
    card: '#ffffff',
    text: '#303233',
    border: '#e8e8e8',
    error: '#f44336',
    notification: '#f44336',
  },
  font: 'Raleway',
  assets: {
    headerBackground: Images.headerBackground,
  },
};
const App = () => {
  const [theme, setTheme] = useState(appTheme);
  useEffect(() => {
    const onChangeTheme = (data: Theme) => {
      setTheme(data);
    };
    const subscription = DeviceEventEmitter.addListener(
      'onChangeTheme',
      onChangeTheme,
    );
    return subscription.remove;
  }, []);

  return (
    <NavigationContainer
      theme={theme}
      screen={Main}
      onDismiss={() => Alert.alert('onDismiss')}
    />
  );
};

const Main: React.FC<ScreenContainerProps> = props => {
  const {navigator} = useContext(NavigationContext);
  const headerRight = (props: any) => (
    <HeaderRightAction {...props}>
      <NavigationButton
        icon="palette-outline"
        onPress={() => {
          navigator?.push({screen: ThemeUsage});
        }}
      />
      <NavigationButton
        icon="palette-outline"
        onPress={() => {
          navigator?.push({screen: ThemeUsage});
        }}
      />
    </HeaderRightAction>
  );

  return (
    <ScreenContainer
      {...props}
      options={{
        title: 'Component Kits',
        headerRight,
      }}
      style={Styles.padding16}>
      <Button
        onPress={() => {
          navigator?.push({
            screen: NavigationUsage,
          });
        }}>
        Navigation
      </Button>
      <SizedBox height={16} />
      <Button
        onPress={() => {
          navigator?.push({
            screen: TextUsage,
          });
        }}>
        Text
      </Button>
      <SizedBox height={16} />
      <Button
        onPress={() => {
          navigator?.push({
            screen: IconButtonUsage,
          });
        }}>
        IconButton
      </Button>
    </ScreenContainer>
  );
};

export default App;
