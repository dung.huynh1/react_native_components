import React, {useContext} from 'react';
import {ScrollView, TouchableOpacity, View} from 'react-native';
import {
  NavigationContext,
  IconButton,
  ScreenContainer,
  ScreenContainerProps,
  Styles,
  Text,
  NavigationButton,
} from '@company/kits';
import Preview from '../../../src/preview';
import {HeaderRightAction} from '@company/kits/Navigation/Components';

const IconButtonUsage: React.FC<ScreenContainerProps> = props => {
  const {theme, navigator} = useContext(NavigationContext);
  const preview = {
    name: {
      title: 'Name',
      direction: 'row',
      value: ['close'],
    },
    type: {
      title: 'Type',
      direction: 'row',
      value: ['primary', 'tonal', 'secondary', 'outline', 'disabled'],
    },
    size: {
      title: 'Size',
      direction: 'row',
      value: ['large', 'small'],
    },
    shape: {
      title: 'Shape',
      direction: 'row',
      value: ['circle', 'rectangle'],
    },
  };

  return (
    <ScreenContainer
      {...props}
      options={{
        title: 'Text',
        headerRight: props => (
          <HeaderRightAction {...props}>
            <NavigationButton
              icon={'home'}
              onPress={() => {
                navigator?.push({
                  screen: () => (
                    <View style={{flex: 1, backgroundColor: 'red'}} />
                  ),
                });
              }}
            />
          </HeaderRightAction>
        ),
      }}>
      <ScrollView
        style={[Styles.flex, {backgroundColor: theme.colors.card}]}
        contentContainerStyle={[
          Styles.paddingHorizontal16,
          Styles.paddingVertical8,
        ]}>
        <Preview component={IconButton} data={preview} />
      </ScrollView>
    </ScreenContainer>
  );
};

export default IconButtonUsage;
