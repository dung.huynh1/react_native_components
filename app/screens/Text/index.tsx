import React, {useContext} from 'react';
import {ScrollView, View} from 'react-native';
import {
  NavigationButton,
  NavigationContext,
  ScreenContainer,
  ScreenContainerProps,
  Styles,
  Text,
} from '@company/kits';
import Preview from '../../../src/preview';
import {HeaderRightAction} from '@company/kits/Navigation/Components';

const TextUsage: React.FC<ScreenContainerProps> = props => {
  const {theme, navigator} = useContext(NavigationContext);
  const preview = {
    fontStyle: {
      title: 'Variants',
      direction: 'row',
      value: ['normal', 'italic'],
    },
    color: {
      title: 'Color',
      direction: 'row',
      value: [theme.colors.primary, theme.colors.secondary, theme.colors.error],
    },
    typography: {
      title: 'Typography',
      direction: 'column',
      value: [
        'h1',
        'h2',
        'h3',
        'h4',
        'title',
        'subtitle',
        'caption',
        'overline',
      ],
    },
    weight: {
      title: 'Font Weight',
      direction: 'column',
      value: [
        'thin',
        'ultraLight',
        'light',
        'regular',
        'medium',
        'semibold',
        'bold',
        'heavy',
        'black',
      ],
    },
  };

  return (
    <ScreenContainer
      {...props}
      options={{
        title: 'Text',
        headerRight: props => (
          <HeaderRightAction {...props}>
            <NavigationButton
              icon="dots-horizontal"
              onPress={() => {
                navigator?.push({
                  screen: () => (
                    <View style={{flex: 1, backgroundColor: 'red'}} />
                  ),
                });
              }}
            />
          </HeaderRightAction>
        ),
      }}>
      <ScrollView
        style={[Styles.flex, {backgroundColor: theme.colors.card}]}
        contentContainerStyle={[
          Styles.paddingHorizontal16,
          Styles.paddingVertical8,
        ]}>
        <Preview component={Text} data={preview} />
      </ScrollView>
    </ScreenContainer>
  );
};

export default TextUsage;
