import React, {useContext, useEffect} from 'react';
import {Alert, View} from 'react-native';
import {
  Button,
  NavigationContainer,
  NavigationContext,
  ScreenContainer,
  ScreenContainerProps,
  SizedBox,
  Styles,
} from '@company/kits';
import {TextUsage} from '../index';

const NavigationUsage: React.FC<ScreenContainerProps> = props => {
  const {navigator, theme} = useContext(NavigationContext);

  useEffect(() => {
    // props.navigation.setOptions({});
  }, []);
  return (
    <ScreenContainer
      {...props}
      options={{title: 'Navigator'}}
      style={Styles.padding16}>
      <Button
        onPress={() => {
          navigator?.push({
            screen: TextUsage,
          });
        }}>
        Push Screen
      </Button>
      <SizedBox height={16} />
      <Button
        onPress={() => {
          navigator?.replace({
            screen: TextUsage,
          });
        }}>
        Replace Screen
      </Button>
      <SizedBox height={16} />
      <Button
        onPress={() => {
          navigator?.present({
            screen: TextUsage,
          });
        }}>
        Present Screen
      </Button>
      <SizedBox height={16} />
      <Button
        onPress={() => {
          const requestClose = (onClose: any) => {
            Alert.alert('onRequestClose', 'My Alert Msg', [
              {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
              {text: 'OK', onPress: onClose},
            ]);
          };
          navigator?.showModal({
            screen: () => {
              return (
                <View style={{height: 400, width: 300}}>
                  <NavigationContainer
                    theme={theme}
                    screen={TextUsage}
                    onDismiss={() => requestClose(navigator.pop)}
                  />
                </View>
              );
            },
            onDismiss: () => {
              Alert.alert('onDismiss');
            },
            onRequestClose: requestClose,
          });
        }}>
        Show Modal
      </Button>
      <SizedBox height={16} />
      <Button
        onPress={() => {
          navigator?.showBottomSheet({
            screen: () => (
              <View style={{height: 500}}>
                <NavigationContainer
                  theme={theme}
                  screen={TextUsage}
                  onDismiss={navigator.pop}
                />
              </View>
            ),
            onDismiss: () => {
              Alert.alert('onDismiss');
            },
          });
        }}>
        Show Bottom Sheet
      </Button>
    </ScreenContainer>
  );
};

export default NavigationUsage;
