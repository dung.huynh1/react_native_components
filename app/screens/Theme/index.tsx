import React, {useContext, useState} from 'react';
import {
  Button,
  Colors,
  defaultDarkTheme,
  defaultTheme,
  // defaultTheme,
  NavigationButton,
  NavigationContext,
  ScreenContainer,
  ScreenContainerProps,
  SizedBox,
  Styles,
  Text,
} from '@company/kits';
import {
  DeviceEventEmitter,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import ColorPicker, {
  HueSlider,
  OpacitySlider,
  Panel1,
  Preview,
} from 'reanimated-color-picker';
import {useSharedValue} from 'react-native-reanimated';
import {HeaderRightAction} from '@company/kits/Navigation/Components';

const ThemeUsage: React.FC<ScreenContainerProps> = props => {
  const {theme, navigator} = useContext(NavigationContext);
  const [darkMode, setDarkMode] = useState(theme.dark);
  const colors: {[key: string]: string} = theme.colors;

  const onDarkChange = (dark: boolean) => {
    let themeColors = defaultTheme.colors;
    if (dark) {
      themeColors = defaultDarkTheme.colors;
    }
    DeviceEventEmitter.emit('onChangeTheme', {
      ...theme,
      dark,
      colors: {
        ...themeColors,
        primary: theme.colors.primary,
        secondary: theme.colors.secondary,
      },
    });
    setDarkMode(dark);
  };

  const onChangeColors = (item: any) => {
    DeviceEventEmitter.emit('onChangeTheme', {
      ...theme,
      colors: item,
    });
  };

  const onChangeColor = (key: string) => {
    const color = colors[key];
    navigator?.showModal({
      screen: () => (
        <View
          style={{
            width: 300,
            height: 350,
            backgroundColor: theme.colors.background,
          }}>
          <Picker
            color={color}
            onChange={(value: string) => {
              colors[key] = value;
              onChangeColors(colors);
            }}
          />
        </View>
      ),
    });
  };

  return (
    <ScreenContainer
      {...props}
      options={{
        title: 'Theme',
        headerRight: props => (
          <HeaderRightAction {...props}>
            <NavigationButton
              {...props}
              icon={'theme-light-dark'}
              onPress={() => {
                onDarkChange(!darkMode);
              }}
            />
          </HeaderRightAction>
        ),
      }}>
      <ScrollView style={Styles.flex} contentContainerStyle={Styles.padding16}>
        {Object.keys(colors).map(item => {
          return (
            <View key={item}>
              <View style={Styles.row}>
                <TouchableOpacity
                  onPress={() => onChangeColor(item)}
                  style={[styles.square, {backgroundColor: colors[item]}]}
                />
                <SizedBox width={8} />
                <Text typography="title" weight="bold">
                  {item}
                </Text>
              </View>
              <SizedBox height={16} />
            </View>
          );
        })}
      </ScrollView>
      <View style={Styles.buttonContent}>
        <Button onPress={() => {}}>Save</Button>
      </View>
    </ScreenContainer>
  );
};

const Picker = (props: any) => {
  const {theme} = useContext(NavigationContext);
  const color = useSharedValue(props?.color ?? theme.colors.primary);

  const onSelectColor = ({hex}: any) => {
    if (hex) {
      props.onChange?.(hex);
    }
  };

  return (
    <ColorPicker
      value={color.value}
      sliderThickness={25}
      thumbSize={30}
      style={styles.pickerContainer}
      onComplete={onSelectColor}>
      <Panel1 style={[styles.panel, Styles.card]} />
      <SizedBox height={16} />
      <View style={Styles.row}>
        <Preview
          style={[styles.previewStyle, Styles.card]}
          hideInitialColor
          hideText
        />
        <View style={Styles.flex}>
          <HueSlider
            thumbShape="triangleDown"
            style={Styles.card}
            thumbColor={theme.colors.border}
          />
          <SizedBox height={16} />
          <OpacitySlider
            thumbShape="triangleUp"
            style={Styles.card}
            thumbColor={theme.colors.border}
          />
        </View>
      </View>
    </ColorPicker>
  );
};

const styles = StyleSheet.create({
  square: {
    width: 36,
    height: 36,
    borderRadius: 12,
    borderWidth: 1,
    borderColor: Colors.grey,
  },
  pickerContainer: {
    flex: 1,
    padding: 16,
    width: '100%',
  },
  panel: {
    height: 200,
  },
  previewStyle: {
    width: 48,
    height: 48,
    borderRadius: 24,
    marginEnd: 16,
  },
});
export default ThemeUsage;
