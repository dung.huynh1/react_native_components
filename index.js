import {GestureHandlerRootView} from 'react-native-gesture-handler';
import React, {useEffect} from 'react';
import {AppRegistry} from 'react-native';
import App from './app';
import {Styles} from '@company/kits';
import i18n from 'i18next';
import {initReactI18next} from 'react-i18next';
import {Setting} from '@configs';

console.warn = message => {
  if (message?.includes('Non-serializable')) {
    return;
  }
  console.warn(message);
};
const Index = () => {
  useEffect(() => {
    i18n
      .use(initReactI18next)
      .init({
        resources: Setting.resourcesLanguage,
        lng: Setting.defaultLanguage,
        fallbackLng: Setting.defaultLanguage,
        compatibilityJSON: 'v3',
      })
      .then(_ => {});
  }, []);
  return (
    <GestureHandlerRootView style={Styles.flex}>
      <App />
    </GestureHandlerRootView>
  );
};

AppRegistry.registerComponent('Runner', () => Index);
