import {View} from 'react-native';
import React, {useContext} from 'react';
import {PreviewProps} from './types';
import styles from './styles';
import {NavigationContext, SizedBox, Styles, Text} from '@company/kits';

const Preview: React.FC<PreviewProps> = ({component, data}) => {
  const {theme} = useContext(NavigationContext);
  const Component = component;
  return (
    <>
      {Object.keys(data).map(key => {
        const item = data[key];
        return (
          <View key={key} style={Styles.paddingVertical8}>
            <Text typography="title" weight="bold">
              {item.title}
            </Text>
            <SizedBox height={8} />
            <View
              style={[
                styles.previewContent,
                {
                  borderColor: theme.colors.border,
                  flexDirection: item.direction,
                },
              ]}>
              {item.value.map((i: string) => {
                let props: {[key: string]: string} = {};
                props[key] = i;
                return (
                  <View key={i} style={Styles.padding4}>
                    <Component {...props}>{i}</Component>
                  </View>
                );
              })}
            </View>
          </View>
        );
      })}
    </>
  );
};

export default Preview;
