import React, {useContext} from 'react';
import {StyleSheet, Text as RNText} from 'react-native';
import {
  NavigationContext,
  FontWeight,
  TextProps,
  Typography,
  TypographyWeight,
} from '../index';
import styles from './styles';

const ProximaNova: TypographyWeight = {
  100: 'Thin',
  200: 'Thin',
  300: 'Thin',
  400: 'Regular',
  500: 'Regular',
  600: 'Regular',
  700: 'Bold',
  800: 'ExtraBold',
  900: 'Black',
  normal: 'Regular',
  bold: 'Bold',
};

const Raleway: TypographyWeight = {
  100: 'Thin',
  200: 'ExtraLight',
  300: 'Light',
  400: 'Regular',
  500: 'Medium',
  600: 'SemiBold',
  700: 'Bold',
  800: 'ExtraBold',
  900: 'Black',
  normal: 'Regular',
  bold: 'Bold',
};

const SFProText: TypographyWeight = {
  100: 'Thin',
  200: 'Ultralight',
  300: 'Light',
  400: 'Regular',
  500: 'Medium',
  600: 'Semibold',
  700: 'Bold',
  800: 'Heavy',
  900: 'Black',
  normal: 'Regular',
  bold: 'Bold',
};

const FontStyle: {[key: string]: string} = {
  normal: '',
  italic: 'Italic',
};

/**
 * export font fontFamily for font
 * @return {*}
 */
const getFontFamily = ({
  fontFamily = 'SFProText',
  fontWeight = '400',
  fontStyle = 'normal',
}) => {
  let style = FontStyle[fontStyle];
  if (style === 'Italic' && fontWeight === '400') {
    fontWeight = 'bold';
  }

  switch (fontFamily) {
    case 'ProximaNova':
      return `${fontFamily}-${ProximaNova[fontWeight]}${style}`;
    case 'Raleway':
      return `${fontFamily}-${Raleway[fontWeight]}${style}`;
    case 'SFProText':
      return `${fontFamily}-${SFProText[fontWeight]}${style}`;
    default:
      return `${fontFamily}-${SFProText[fontWeight]}${style}`;
  }
};
const Text: React.FC<TextProps> = ({
  typography,
  weight,
  color,
  fontStyle,
  children,
  style,
  ...rest
}) => {
  const {theme} = useContext(NavigationContext);

  /**
   * typography style
   * @return {*}
   */
  const getTypography = (value: Typography) => {
    switch (value) {
      case 'h1':
        return styles.h1;
      case 'h2':
        return styles.h2;
      case 'h3':
        return styles.h3;
      case 'h4':
        return styles.h4;
      case 'title':
        return styles.title;
      case 'subtitle':
        return styles.subtitle;
      case 'caption':
        return styles.caption;
      case 'overline':
        return styles.overline;
      default:
        return styles.title;
    }
  };

  /**
   * weight style
   * @return {*}
   */
  const getFontWeight = (value: FontWeight | undefined) => {
    switch (value) {
      case 'thin':
        return {fontWeight: '100'};
      case 'ultraLight':
        return {fontWeight: '200'};
      case 'light':
        return {fontWeight: '300'};
      case 'regular':
        return {fontWeight: '400'};
      case 'medium':
        return {fontWeight: '500'};
      case 'semibold':
        return {fontWeight: '600'};
      case 'bold':
        return {fontWeight: '700'};
      case 'heavy':
        return {fontWeight: '800'};
      case 'black':
        return {fontWeight: '900'};
      default:
        return {fontWeight: '400'};
    }
  };

  const textStyle: any = StyleSheet.flatten([
    {fontFamily: theme.font, color: color ?? theme.colors.text},
    getTypography(typography),
    getFontWeight(weight),
    style,
  ]);

  textStyle.fontFamily = getFontFamily({
    fontFamily: textStyle.fontFamily,
    fontWeight: textStyle.fontWeight,
    fontStyle,
  });

  return (
    <RNText {...rest} style={textStyle}>
      {children ?? ''}
    </RNText>
  );
};

Text.defaultProps = {
  typography: 'title',
  weight: 'regular',
  color: undefined,
  fontStyle: 'normal',
};

export {Text, getFontFamily};
