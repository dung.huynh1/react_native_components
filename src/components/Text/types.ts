import {TextProps as RNTextProps} from 'react-native';

export interface TypographyWeight {
  [key: string]: string;
  100: string;
  200: string;
  300: string;
  400: string;
  500: string;
  600: string;
  700: string;
  800: string;
  900: string;
  normal: string;
  bold: string;
}

export type Typography =
  | 'h1'
  | 'h2'
  | 'h3'
  | 'h4'
  | 'title'
  | 'subtitle'
  | 'caption'
  | 'overline';

export type FontWeight =
  | 'thin'
  | 'ultraLight'
  | 'light'
  | 'regular'
  | 'medium'
  | 'semibold'
  | 'bold'
  | 'heavy'
  | 'black';

export type FontStyle = 'normal' | 'italic';
export interface TextProps extends RNTextProps {
  typography: Typography;
  weight?: FontWeight;
  color?: string;
  fontStyle?: FontStyle;
}
