import React, {useContext} from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {NavigationContext, Colors, Icon, IconButtonProps} from '../index';

const IconButton: React.FC<IconButtonProps> = ({
  style,
  type,
  icon,
  size,
  shape,
  ...rest
}) => {
  const {theme} = useContext(NavigationContext);

  /**
   * export size style
   */
  const getSizeStyle = () => {
    switch (size) {
      case 'large':
        return styles.large;
      case 'small':
        return styles.small;

      default:
        return styles.small;
    }
  };

  /**
   * export size style
   */
  const getIconSize = () => {
    switch (size) {
      case 'large':
        return 24;
      case 'small':
        return 16;

      default:
        return 16;
    }
  };

  /**
   * export type style
   */
  const getTypeStyle = () => {
    switch (type) {
      case 'primary':
        return {backgroundColor: theme.colors.primary};
      case 'tonal':
        return {backgroundColor: theme.colors.primary + '33'};
      case 'secondary':
        return {borderColor: theme.colors.border, borderWidth: 1};
      case 'outline':
        return {
          borderWidth: 1,
          borderColor: theme.colors.primary,
        };
      case 'disabled':
        return {backgroundColor: theme.colors.border};
      default:
        return {backgroundColor: theme.colors.primary};
    }
  };

  const getIconColor = () => {
    switch (type) {
      case 'primary':
        return Colors.white;
      case 'tonal':
        return theme.colors.primary;
      case 'secondary':
        return theme.colors.text;
      case 'outline':
        return theme.colors.primary;
      case 'disabled':
        return theme.colors.text + '4D';
      default:
        return Colors.white;
    }
  };

  const buttonStyle = StyleSheet.flatten([
    getSizeStyle(),
    getTypeStyle(),
    shape === 'rectangle' && {borderRadius: 8},
    style,
  ]);

  return (
    <TouchableOpacity
      {...rest}
      disabled={type === 'disabled'}
      style={buttonStyle}>
      <Icon name={icon} size={getIconSize()} color={getIconColor()} />
    </TouchableOpacity>
  );
};

IconButton.defaultProps = {
  icon: 'close',
  type: 'primary',
  size: 'small',
  shape: 'circle',
  children: <View />,
};

const styles = StyleSheet.create({
  large: {
    height: 48,
    width: 48,
    borderRadius: 24,
    justifyContent: 'center',
    alignItems: 'center',
  },
  small: {
    height: 36,
    width: 36,
    borderRadius: 18,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export {IconButton};
