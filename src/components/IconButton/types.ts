import {TouchableOpacityProps} from 'react-native';

export type IconButtonType =
  | 'primary'
  | 'tonal'
  | 'secondary'
  | 'outline'
  | 'disabled';
export type IconButtonSize = 'large' | 'small';
export type IconButtonShape = 'circle' | 'rectangle';

export interface IconButtonProps extends TouchableOpacityProps {
  icon: string;
  type?: IconButtonType;
  size?: IconButtonSize;
  shape?: IconButtonShape;
}
