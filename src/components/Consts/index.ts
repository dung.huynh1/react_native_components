import {Context, Theme} from '../Navigation/types';

const defaultTheme: Theme = {
  dark: false,
  colors: {
    primary: '#eb2f96',
    secondary: '#007aff',
    background: '#f9f9f9',
    card: '#ffffff',
    text: '#303233',
    border: '#e8e8e8',
    error: '#f44336',
    notification: '#f44336',
  },
  font: 'Raleway',
};

const defaultDarkTheme: Theme = {
  dark: true,
  colors: {
    primary: '#eb2f96',
    secondary: '#007aff',
    background: '#171819',
    card: '#202122',
    text: '#e4e6eb',
    border: '#333435',
    error: '#f44336',
    notification: '#f44336',
  },
  font: 'Raleway',
};

const defaultContext: Context = {
  theme: defaultTheme,
  navigator: undefined,
};

export {defaultContext, defaultTheme, defaultDarkTheme};
export * from './colors+spacing+radius';
export * from './styles';
