import React, {useContext} from 'react';
import {I18nManager, StyleSheet} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Foundation from 'react-native-vector-icons/Foundation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Octicons from 'react-native-vector-icons/Octicons';

import {NavigationContext, IconProps} from '../index';

const Icon: React.FC<IconProps> = ({style, type, ...rest}) => {
  const {theme} = useContext(NavigationContext);

  let Component;
  switch (type) {
    case 'AntDesign':
      Component = AntDesign;
      break;
    case 'Entypo':
      Component = Entypo;
      break;
    case 'EvilIcons':
      Component = EvilIcons;
      break;
    case 'Feather':
      Component = Feather;
      break;
    case 'FontAwesome':
      Component = FontAwesome;
      break;
    case 'FontAwesome5':
      Component = FontAwesome5;
      break;
    case 'Fontisto':
      Component = Fontisto;
      break;
    case 'Foundation':
      Component = Foundation;
      break;
    case 'Ionicons':
      Component = Ionicons;
      break;
    case 'MaterialCommunityIcons':
      Component = MaterialCommunityIcons;
      break;
    case 'MaterialIcons':
      Component = MaterialIcons;
      break;
    case 'Octicons':
      Component = Octicons;
      break;
    default:
      Component = MaterialCommunityIcons;
      break;
  }
  return (
    <Component
      color={theme.colors.text}
      {...rest}
      style={[styles.styleRTL, style]}
    />
  );
};

Icon.defaultProps = {
  name: 'help-circle-outline',
  size: 24,
  type: 'MaterialCommunityIcons',
};

const styles = StyleSheet.create({
  styleRTL: {transform: [{scaleX: I18nManager.isRTL ? -1 : 1}]},
});

export {Icon};
