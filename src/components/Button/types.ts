import {
  TouchableNativeFeedbackProps,
  TouchableOpacityProps,
} from 'react-native';

export type ButtonType =
  | 'primary'
  | 'secondary'
  | 'tonal'
  | 'outline'
  | 'disable'
  | 'text';

export type ButtonSize = 'large' | 'medium' | 'small';

export interface ButtonProps
  extends TouchableOpacityProps,
    TouchableNativeFeedbackProps {}
