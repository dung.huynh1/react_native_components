import React, {useContext, useEffect, useMemo, useRef} from 'react';
import {Animated, Platform, useWindowDimensions, View} from 'react-native';
import {
  ContentLoaderTypes,
  LinearGradient,
  NavigationContext,
  Styles,
} from '../index';
import styles from './styles';

const ContentLoader: React.FC<ContentLoaderTypes> = props => {
  const {width} = useWindowDimensions();
  const {theme} = useContext(NavigationContext);
  const beginShimmerPosition = useRef(new Animated.Value(-1)).current;
  const {style} = props;

  const shimmerColors = [
    theme.colors.border + '40',
    theme.colors.border + '80',
    theme.colors.border,
  ];
  const location = [0.3, 0.5, 0.7];
  const linearTranslate = beginShimmerPosition.interpolate({
    inputRange: [-1, 1],
    outputRange: [0, width],
  });
  const animatedValue = useMemo(() => {
    return Animated.loop(
      Animated.timing(beginShimmerPosition, {
        toValue: 1,
        delay: 0,
        duration: 1000,
        useNativeDriver: Platform.OS !== 'web',
      }),
    );
  }, [beginShimmerPosition]);

  useEffect(() => {
    animatedValue.start();
    return () => {
      animatedValue.stop();
    };
  }, [animatedValue]);

  return (
    <View style={[styles.container, style]}>
      <View style={[Styles.flex, {backgroundColor: shimmerColors[0]}]}>
        <Animated.View
          style={[Styles.flex, {transform: [{translateX: linearTranslate}]}]}>
          <LinearGradient
            colors={shimmerColors}
            style={[Styles.flex, {width: width}]}
            start={{
              x: -1,
              y: 0.5,
            }}
            end={{
              x: 2,
              y: 0.5,
            }}
            locations={location}
          />
        </Animated.View>
      </View>
    </View>
  );
};

ContentLoader.defaultProps = {};

export {ContentLoader};
