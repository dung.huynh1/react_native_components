import {
  defaultContext,
  SafeAreaProvider,
  NavigationContainerProps,
} from '../index';
import React, {createContext, useRef} from 'react';
import {BottomSheetModalProvider} from '@gorhom/bottom-sheet';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {
  createNavigationContainerRef,
  NavigationContainer as ReactNavigationContainer,
} from '@react-navigation/native';
import StackScreen from './StackScreen';
import ModalScreen from './ModalScreen';
import Navigator from './Navigator';
import {getDialogOptions, getModalOptions, getStackOptions} from './utils';

const Stack = createNativeStackNavigator();

const NavigationContext = createContext(defaultContext);
const NavigationContainer: React.FC<NavigationContainerProps> = ({
  screen,
  theme,
  onDismiss,
}) => {
  const navigator = useRef(new Navigator(createNavigationContainerRef()));

  const goBack = () => {
    const canGoBack = navigator.current?.ref.canGoBack();
    if (canGoBack) {
      navigator.current?.ref.goBack();
    } else {
      onDismiss?.();
    }
  };

  return (
    <SafeAreaProvider>
      <BottomSheetModalProvider>
        <NavigationContext.Provider
          value={{theme, navigator: navigator.current}}>
          <ReactNavigationContainer
            theme={theme}
            ref={navigator.current?.ref}
            independent={true}>
            <Stack.Navigator initialRouteName="Stack">
              <Stack.Screen
                name="Stack"
                component={StackScreen}
                initialParams={{screen}}
                options={getStackOptions(theme, goBack)}
              />
              <Stack.Screen
                name="Dialog"
                component={StackScreen}
                options={getDialogOptions(theme, goBack)}
                initialParams={{screen}}
              />
              <Stack.Screen
                name="Modal"
                component={ModalScreen}
                options={getModalOptions()}
                initialParams={{screen}}
              />
            </Stack.Navigator>
          </ReactNavigationContainer>
        </NavigationContext.Provider>
      </BottomSheetModalProvider>
    </SafeAreaProvider>
  );
};

export {NavigationContext, NavigationContainer};
