import React from 'react';
import {ScreenParams} from './types';
import Navigation from './Navigation';

const StackScreen: React.FC<any> = props => {
  const {route, navigation} = props;
  const {screen: Component}: ScreenParams = route.params;

  const params = {
    ...route.params,
    navigation: new Navigation(navigation),
  };

  delete params.screen;

  return <Component {...params} />;
};

export default StackScreen;
