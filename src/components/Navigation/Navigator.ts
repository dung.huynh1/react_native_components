import {StackActions} from '@react-navigation/native';
import {ModalParams, ScreenParams} from './types';

class Navigator {
  ref?: any;

  constructor(navigation: any) {
    this.ref = navigation;
  }

  push = (params: ScreenParams) => {
    if (this.ref?.isReady()) {
      this.ref.dispatch(StackActions.push('Stack', params));
    }
  };

  replace = (params: ScreenParams) => {
    if (this.ref?.isReady()) {
      this.ref.dispatch(StackActions.replace('Stack', params));
    }
  };

  pop = (count?: number) => {
    if (this.ref?.isReady()) {
      this.ref.dispatch(StackActions.pop(count ?? 1));
    }
  };

  present = (params: ScreenParams) => {
    if (this.ref?.isReady()) {
      this.ref.dispatch(StackActions.push('Dialog', params));
    }
  };

  showModal = (params: ModalParams) => {
    if (this.ref?.isReady()) {
      this.ref.dispatch(StackActions.push('Modal', params));
    }
  };

  showBottomSheet = (params: ModalParams) => {
    if (this.ref?.isReady()) {
      this.ref.dispatch(
        StackActions.push('Modal', {
          ...params,
          isBottomSheet: true,
        }),
      );
    }
  };

  popToTop = () => {
    if (this.ref.isReady()) {
      this.ref.dispatch(StackActions.popToTop());
    }
  };
}

export default Navigator;
