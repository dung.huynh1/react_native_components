import {NavigationProp} from '@react-navigation/native';
import {NavigationOptions} from './types';
import {HeaderRightAction} from './Components';

class Navigation {
  instance: NavigationProp<any>;

  constructor(instance: any) {
    this.instance = instance;
  }

  verifyParams = (params: NavigationOptions) => {
    if (params.headerRight) {
      const headerRight = params.headerRight?.();
      if (headerRight.type !== HeaderRightAction) {
        console.error(
          'headerRight not return element type of HeaderRightAction, Please migrate to use HeaderRightAction of kits.',
        );
      }
    }
  };
  setOptions = (params: NavigationOptions) => {
    this.verifyParams(params);
    this.instance.setOptions(params);
  };
}

export default Navigation;
