import {KeyboardAvoidingView, Platform} from 'react-native';
import {useHeaderHeight} from '@react-navigation/elements';
import React, {useLayoutEffect} from 'react';
import {SafeAreaView, ScreenContainerProps, Styles} from '../index';

const ScreenContainer: React.FC<ScreenContainerProps> = ({
  style,
  children,
  navigation,
  options,
  edges = ['bottom', 'left', 'right'],
  enableKeyboardAvoidingView = false,
}) => {
  const headerHeight = useHeaderHeight();
  useLayoutEffect(() => {
    if (options) {
      navigation.setOptions(options);
    }
  }, [navigation, options]);

  return (
    <SafeAreaView style={[Styles.flex, style]} edges={edges}>
      <KeyboardAvoidingView
        style={Styles.flex}
        keyboardVerticalOffset={headerHeight}
        enabled={enableKeyboardAvoidingView}
        behavior={Platform.select({
          ios: 'padding',
          android: undefined,
        })}>
        {children}
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

export default ScreenContainer;
