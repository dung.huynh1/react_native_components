import React, {useContext} from 'react';
import {StyleSheet, View} from 'react-native';
import {Image, NavigationButton, NavigationContext, Styles} from '../index';

const styles = StyleSheet.create({
  headerBackground: {
    width: '100%',
    height: undefined,
    position: 'absolute',
    aspectRatio: 375 / 154,
  },
});

const BackButton: React.FC<any> = props => {
  return (
    <View style={Styles.headerLeftButton}>
      <NavigationButton icon="arrow-left" {...props} />
    </View>
  );
};

const HeaderBackground: React.FC<any> = () => {
  const {theme} = useContext(NavigationContext);
  return (
    <View style={[Styles.flex, {backgroundColor: theme.colors.background}]}>
      <Image
        style={styles.headerBackground}
        source={theme.assets?.headerBackground}
      />
    </View>
  );
};

const HeaderRightAction: React.FC<any> = ({children, ...restProps}) => {
  const validType = (item: any) => {
    if (item.type !== NavigationButton) {
      console.error(
        'element type of NavigationButton, Please migrate to use NavigationButton of kits.',
      );
    }
  };
  const renderAction = () => {
    if (Array.isArray(children)) {
      return children.map((child, index) => {
        validType(child);
        return (
          <React.Fragment key={index}>
            {React.cloneElement(child, {...restProps})}
          </React.Fragment>
        );
      });
    }

    validType(children);
    return React.cloneElement(children, {...restProps});
  };
  return <View style={Styles.row}>{renderAction()}</View>;
};

export {BackButton, HeaderBackground, HeaderRightAction};
