import React from 'react';
import {Colors, Text, Theme} from '../index';
import {NativeStackNavigationOptions} from '@react-navigation/native-stack';
import {BackButton, HeaderBackground} from './Components';

const renderTitle = (props: any) => {
  return (
    <Text
      {...props}
      typography="h4"
      weight="bold"
      style={{color: props.tintColor}}
    />
  );
};

const getStackOptions = (
  theme: Theme,
  goBack: () => void,
): NativeStackNavigationOptions => {
  const getHeaderBackground = (): any => {
    if (theme.assets?.headerBackground) {
      return {
        headerBackground: HeaderBackground,
      };
    }
  };

  return {
    statusBarStyle: 'light',
    headerBackTitleVisible: false,
    headerTintColor: Colors.white,
    headerTitleAlign: 'center',
    headerTitle: renderTitle,
    headerLeft: props => <BackButton {...props} onPress={goBack} />,
    ...getHeaderBackground(),
  };
};

const getDialogOptions = (
  theme: Theme,
  goBack: () => void,
): NativeStackNavigationOptions => {
  return {
    presentation: 'containedModal',
    headerBackground: undefined,
    statusBarStyle: 'dark',
    headerBackTitleVisible: false,
    headerTintColor: theme.colors.text,
    headerTitleAlign: 'center',
    headerTitle: renderTitle,
    headerLeft: props => (
      <BackButton {...props} icon="close" onPress={goBack} />
    ),
  };
};

const getModalOptions = (): NativeStackNavigationOptions => {
  return {
    headerShown: false,
    headerBackground: undefined,
    presentation: 'containedTransparentModal',
    animation: 'fade',
    contentStyle: {backgroundColor: Colors.modal},
  };
};

export {getStackOptions, getDialogOptions, getModalOptions};
