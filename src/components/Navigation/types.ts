import {ViewStyle} from 'react-native';
import React, {ReactNode} from 'react';
import Navigator from './Navigator';
import {Source} from 'react-native-fast-image';
import Navigation from './Navigation';
import NavigationButton from './NavigationButton';
import {HeaderButtonProps} from '@react-navigation/native-stack/src/types';

export type Theme = {
  dark: boolean;
  colors: {
    primary: string;
    secondary: string;
    background: string;
    card: string;
    text: string;
    border: string;
    error: string;
    notification: string;
  };
  font: string;
  assets?: {
    headerBackground?: Source;
  };
};

export type Context = {
  theme: Theme;
  navigator?: Navigator;
};

export type NavigationContainerProps = {
  screen: React.ElementType;
  theme: Theme;
  onDismiss: () => void;
};

export type ScreenContainerProps = {
  style?: ViewStyle;
  children: ReactNode;
  navigation: Navigation;
  options?: NavigationOptions;
  edges?: any[];
  enableKeyboardAvoidingView?: boolean;
};

export type ScreenParams = {
  [key: string]: any;
  screen: React.ElementType;
  options?: NavigationOptions;
};

export interface ModalParams extends ScreenParams {
  [key: string]: any;
  screen: React.ElementType;
  onDismiss?: (mounted?: boolean) => void;
  onRequestClose?: (onClose: () => void) => void;
}

export type NavigationButtonProps = {
  icon: string;
  tintColor?: string;
  onPress: () => void;
};

export type NavigationOptions = {
  title?: string;
  headerBackground?: string;
  headerRight?: (props?: HeaderButtonProps) => React.ReactElement;
};
