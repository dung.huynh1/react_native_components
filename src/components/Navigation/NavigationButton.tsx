import {StyleSheet, TouchableOpacity} from 'react-native';
import React, {useContext} from 'react';
import {Icon, NavigationContext, NavigationButtonProps} from '../index';

const NavigationButton: React.FC<NavigationButtonProps> = props => {
  const {icon, tintColor, onPress} = props;
  const {theme} = useContext(NavigationContext);
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <Icon name={icon} color={tintColor ?? theme.colors.text} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 36,
    width: 36,
    borderRadius: 18,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default NavigationButton;
