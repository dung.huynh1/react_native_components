import React, {useContext, useEffect, useMemo, useRef} from 'react';
import {
  KeyboardAvoidingView,
  Platform,
  Pressable,
  StyleSheet,
  View,
} from 'react-native';
import {
  NavigationContext,
  Radius,
  SafeAreaView,
  Styles,
  useSafeAreaInsets,
  ModalParams,
} from '../index';
import {
  BottomSheetBackdrop,
  BottomSheetModal,
  BottomSheetView,
  useBottomSheetDynamicSnapPoints,
} from '@gorhom/bottom-sheet';
import {BottomSheetModalMethods} from '@gorhom/bottom-sheet/lib/typescript/types';
import Navigation from './Navigation';

const ModalScreen: React.FC<any> = props => {
  const {navigation, route} = props;
  const {isBottomSheet, screen, onRequestClose}: ModalParams = route.params;
  const Component = useRef(screen).current;
  const params = {
    ...route.params,
    navigation: new Navigation(navigation),
  };
  delete params.screen;

  /**
   * onDismiss modal
   */
  const onDismiss = (pop = true) => {
    let action = navigation.pop;
    if (!pop) {
      action = () => {};
    }
    if (typeof onRequestClose === 'function' && !isBottomSheet) {
      onRequestClose(() => {
        params?.onDismiss?.();
        action();
      });
      return;
    }
    params?.onDismiss?.();
    action();
  };

  /**
   * build bottom sheet
   */
  if (isBottomSheet) {
    return (
      <BottomSheet {...props} onDismiss={onDismiss}>
        <Component {...params} />
      </BottomSheet>
    );
  }

  return (
    <KeyboardAvoidingView
      style={Styles.flex}
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
      <View style={styles.container}>
        <Pressable
          style={styles.modalSpaceHorizontal}
          onPress={() => onDismiss()}
        />
        <View style={styles.modalCenterStage}>
          <Pressable
            style={styles.modalSpaceVertical}
            onPress={() => onDismiss()}
          />
          <View style={styles.modalContent}>
            <Component {...params} />
          </View>
          <Pressable
            style={styles.modalSpaceVertical}
            onPress={() => onDismiss()}
          />
        </View>
        <Pressable
          style={styles.modalSpaceHorizontal}
          onPress={() => onDismiss()}
        />
      </View>
    </KeyboardAvoidingView>
  );
};

const BottomSheet: React.FC<any> = props => {
  const {children, onDismiss}: ModalParams = props;
  const {theme} = useContext(NavigationContext);
  const initialSnapPoints = useMemo(() => ['CONTENT_HEIGHT'], []);
  const {bottom} = useSafeAreaInsets();
  const bottomSheetRef = useRef<BottomSheetModalMethods>(null);
  const mountedRef = useRef(true);
  const {
    animatedHandleHeight,
    animatedSnapPoints,
    animatedContentHeight,
    handleContentLayout,
  } = useBottomSheetDynamicSnapPoints(initialSnapPoints);

  useEffect(() => {
    bottomSheetRef.current?.present();
    return () => {
      mountedRef.current = false;
    };
  }, []);

  const handleComponent = () => (
    <View style={styles.indicatorContainer}>
      <View style={[styles.indicator, {backgroundColor: theme.colors.card}]} />
    </View>
  );
  const backdropComponent = (backdropProps: any) => (
    <BottomSheetBackdrop
      disappearsOnIndex={-1}
      appearsOnIndex={0}
      {...backdropProps}
    />
  );

  return (
    <SafeAreaView style={Styles.flex}>
      <BottomSheetModal
        ref={bottomSheetRef}
        backgroundStyle={{backgroundColor: theme.colors.card}}
        snapPoints={animatedSnapPoints}
        handleHeight={animatedHandleHeight}
        contentHeight={animatedContentHeight}
        keyboardBehavior="interactive"
        keyboardBlurBehavior="none"
        stackBehavior="push"
        android_keyboardInputMode="adjustResize"
        onDismiss={() => onDismiss?.(mountedRef.current)}
        handleComponent={handleComponent}
        backdropComponent={backdropComponent}>
        <BottomSheetView
          onLayout={handleContentLayout}
          style={[styles.bottomSheetContainer, {paddingBottom: bottom}]}>
          {children}
        </BottomSheetView>
      </BottomSheetModal>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {flex: 1, flexDirection: 'row'},
  modalContent: {
    borderRadius: Radius.M,
    overflow: 'hidden',
    maxWidth: '100%',
    maxHeight: '80%',
  },
  modalCenterStage: {
    maxWidth: '90%',
  },
  modalSpaceHorizontal: {
    flex: 1,
    minWidth: '5%',
  },
  modalSpaceVertical: {
    flex: 1,
    minHeight: '10%',
  },
  bottomSheetContainer: {
    borderTopLeftRadius: Radius.M,
    borderTopRightRadius: Radius.M,
    overflow: 'hidden',
  },
  indicatorContainer: {
    width: '100%',
    height: 4,
    position: 'absolute',
    top: -8,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  indicator: {
    width: 64,
    height: '100%',
    borderRadius: Radius.S,
  },
});

export default ModalScreen;
