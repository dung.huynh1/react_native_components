import {FastImageProps} from 'react-native-fast-image';

export interface ImageProps extends FastImageProps {
  placeholder?: boolean;
  error?: boolean;
}
