import React, {memo, useContext, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import {
  ContentLoader,
  Icon,
  NavigationContext,
  Styles,
  Text,
  ImageProps,
} from '../index';
import styles from './styles';

const ImageComponent: React.FC<ImageProps> = props => {
  const {theme} = useContext(NavigationContext);
  const {style, placeholder, error, source} = props;
  const [loading, setLoading] = useState(typeof source === 'object');
  const [fail, setFail] = useState(false);

  /**
   * render content
   * @returns {JSX.Element}
   */
  const renderContent = () => {
    if (loading || fail) {
      let content = placeholder ?? (
        <ContentLoader style={[StyleSheet.absoluteFill, styles.image]} />
      );
      if (fail) {
        content = error ?? (
          <View
            style={[Styles.flexCenter, {backgroundColor: theme.colors.border}]}>
            <Icon name="error-outline" />
            <Text typography="subtitle" style={Styles.textCenter}>
              Can't load image
            </Text>
          </View>
        );
      }
      return (
        <View style={[StyleSheet.absoluteFill, styles.image]}>{content}</View>
      );
    }
  };

  return (
    <View style={[styles.container, style]}>
      <FastImage
        {...props}
        style={styles.image}
        onLoad={() => {
          setFail(false);
          setLoading(true);
        }}
        onLoadEnd={() => setLoading(false)}
        onError={() => setFail(true)}
      />
      {renderContent()}
    </View>
  );
};

ImageComponent.defaultProps = {
  style: {},
};

const Image = memo(ImageComponent);

export {Image};
