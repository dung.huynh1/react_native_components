import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  previewContent: {
    padding: 16,
    borderRadius: 12,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
  },
});
