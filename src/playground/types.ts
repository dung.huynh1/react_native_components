import React from 'react';

export interface PlaygroundProps {
  component: React.ElementType;
  data: any;
  defaultProps: any;
}
