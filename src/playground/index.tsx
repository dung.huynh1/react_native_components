import {View} from 'react-native';
import React, {useContext, useState} from 'react';
import {PlaygroundProps} from './types';
import {NavigationContext, SizedBox, Styles, Text} from '@company/kits';
import styles from './styles';

const Playground: React.FC<PlaygroundProps> = ({
  component,
  data,
  defaultProps,
}) => {
  const {theme} = useContext(NavigationContext);
  const Component: any = component;

  const [state, setState] = useState(defaultProps);

  return (
    <View style={Styles.paddingVertical8}>
      <Text typography="title" weight="bold">
        Playground
      </Text>
      <SizedBox height={8} />
      <View
        style={[
          styles.previewContent,
          {
            borderColor: theme.colors.border,
          },
        ]}>
        <Component {...state} />
      </View>
      {Object.keys(data).map(key => {
        const item = data[key];
        return <View key={key} style={Styles.paddingVertical8}></View>;
      })}
    </View>
  );
};

export default Playground;
